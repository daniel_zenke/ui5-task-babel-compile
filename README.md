# ui5-task-babel-compile

Task extension for [UI5 Tooling](https://sap.github.io/ui5-tooling/) which converts javascript code to support older browsers. It's based on Wouter Lemaire's blog post [UI5 Tooling Custom Transpiler (Babel) Builder Extension Task](https://blogs.sap.com/2019/08/28/ui5-tooling-custom-transpiler-babel-builder-extension-task/).

## Getting started

### Install

Install this package using your favorite package manager. (e.g. [pnpm<img src="https://pbs.twimg.com/profile_images/875481448517644288/JLVo_YPe_400x400.jpg" alt="drawing" height="32px"/>](https://www.npmjs.com/package/pnpm))

```
pnpm i -D ui5-task-babel-compile
```

### Configuration

In your `package.json` add the task as a ui5 dependency.

```json
{
  "ui5": {
    "dependencies": ["ui5-task-babel-compile"]
  }
}
```

Last but not least add the task as a custom task in your `ui5.yaml`.

```yaml
builder:
  customTasks:
    - name: ui5-task-babel-compile
      afterTask: replaceVersion
```
