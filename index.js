const babel = require("@babel/core");
const pathregen = require("regenerator-runtime/path").path;
const resourceFactory = require("@ui5/fs").resourceFactory;
const fs = require("fs");
const baseLogTask = "info babel:";

/**
 * Custom task example
 *
 * @param {Object} parameters Parameters
 * @param {module:@ui5/fs.DuplexCollection} parameters.workspace DuplexCollection to read and write files
 * @param {module:@ui5/fs.AbstractReader} parameters.dependencies Reader or Collection to read dependency files
 * @param {Object} parameters.options Options
 * @param {string} parameters.options.projectName Project name
 * @param {string} [parameters.options.configuration] Task configuration if given in ui5.yaml
 * @returns {Promise<undefined>} Promise resolving with <code>undefined</code> once data has been written
 */
module.exports = async function({ workspace, dependencies, options }) {
  const babelPrefix = getBabelPath();
  const jsResources = await workspace.byGlob("**/*.js");
  const modules = "node_modules";
  console.info(`${baseLogTask} ${"add regenerator-runtime"}`);
  // get path of component
  const componentResource = jsResources.find(jsResources =>
    jsResources.getPath().includes("Component.js")
  );
  const toPath = componentResource.getPath();
  const pathPrefix = toPath.replace("Component.js", "");
  // get path of regenerator in node_modules
  const pathRegenerator = pathregen
    .substr(pathregen.lastIndexOf(modules) + (modules.length + 1))
    .replace(/\\/g, "/");
  // build full path for regenerator in current project
  const virtualPathRegenerator = pathPrefix + pathRegenerator;
  // get code of regenerator
  const runtimeCode = fs.readFileSync(pathregen, "utf-8");
  // create resource
  const runtimeResource = resourceFactory.createResource({
    path: virtualPathRegenerator,
    string: runtimeCode
  });
  // save regenerator to workspace
  await workspace.write(runtimeResource);

  console.info(`${baseLogTask} Include regenerator-runtime`);
  // add require regenerator for development purpose
  let componentSource = await componentResource.getString();
  const requirePath = virtualPathRegenerator
    .replace("/resources/", "")
    .replace(".js", "");
  componentSource =
    "// development mode: load the regenerator runtime synchronously\nif(!window.regeneratorRuntime){sap.ui.requireSync('" +
    requirePath +
    "')}" +
    componentSource;
  componentResource.setString(componentSource);
  await workspace.write(componentResource);

  console.info(`${baseLogTask} Start transformation`);
  const filteredResources = jsResources.filter(resource => {
    return !resource.getPath().includes("/libs/");
  });

  const transformCode = async resource => {
    const source = await resource.getString();
    console.info(`${baseLogTask} Transforming: ${resource.getPath()}`);
    const { code, map, ast } = babel.transformSync(source, {
      presets: [`${babelPrefix}@babel/preset-env`],
      plugins: [`${babelPrefix}@babel/plugin-transform-modules-commonjs`]
    });
    resource.setString(code);
    return resource;
  };

  const transformedResources = await Promise.all(
    filteredResources.map(resource => transformCode(resource))
  );
  console.info(`${baseLogTask} Transformation finished`);

  console.info(`${baseLogTask} Start updating files`);
  await Promise.all(
    transformedResources.map(resource => {
      return workspace.write(resource);
    })
  );

  console.info(`${baseLogTask} Updating files finished`);
  console.info(`${baseLogTask} Babel task finished`);
};

function getBabelPath() {
  const pnpmPath = ".pnpm/node_modules/";
  const path = `${process.cwd().replace(/\\/g, "/")}/node_modules/${pnpmPath}/`;
  const isPnpm = fs.existsSync(path);
  if(isPnpm) return path;
  return "";
}
